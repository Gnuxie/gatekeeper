#| This file is part of gatekeeper
   Copyright (C) 2019 Gnuxie <Gnuxie@protonmail.com>|#

(in-package #:gatekeeper.test)

(defvar *errorp* nil)
(defvar *last-content* "")

(setf hunchentoot:*catch-errors-p* nil)

(hunchentoot:define-easy-handler (request-token :uri "/registration/request_token" :default-request-type :post) ()
  (setf *last-content* (jsown:parse (hunchentoot:raw-post-data :force-text t)))
  (setf (hunchentoot:content-type*) "application/json")
  (if *errorp*
      (format nil "{\"error\" : \"fak it exploded\"}")
      (format nil "{\"error\" : false, \"url\" : \"https://foo.net/pooptown\"}")))

(hunchentoot:start (make-instance 'hunchentoot:easy-acceptor :port 4242))
