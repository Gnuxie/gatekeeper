#| This file is part of gatekeeper
   Copyright (C) 2019 Gnuxie <Gnuxie@protonmail.com>|#

(in-package #:gatekeeper.test)

(setf luna.framework.hooks:*bot-name* "!gatekeeper")
(push "localhost" gatekeeper:*homeserver-whitelist*)
(setf gatekeeper::*target-endpoint* "http://localhost:4242/registration/request_token")

(define-test invite

  (with-fixtures '(luna.framework:*debug-execution*)
    (setf luna.framework:*debug-execution* t)
    (cl-matrix:with-account (luna.test:*normal-user*)
      (let* ((room (cl-matrix:room-create))
             (listener
              (cl-matrix:with-account (luna.test:*luna-user*)
                (cl-matrix:room-join room)
                (luna:start-listening :sync-rate 2))))

        (push listener luna.test:*listeners*)
        (cl-matrix:msg-send "!gatekeeper invite 5" room)
        (true (luna.test:wait-until
               room
               (lambda (e)
                 (and
                  (string= (cl-matrix:username luna.test:*luna-user*)  (jsown:val e "sender"))
                  (string= "https://foo.net/pooptown" (jsown:filter e "content" "body"))))))

        (is = 5 (jsown:val *last-content* "count"))
        (is string= (subseq (car (cl-strings:split (cl-matrix:username luna.test:*normal-user*) ":")) 1)
            (jsown:val *last-content* "localpart")
            "localpart incorrect in the posted content.")

        ))))

(define-test invite-hook

  (with-fixtures '(luna.framework:*debug-execution*)
    (setf luna.framework:*debug-execution* t)
    (cl-matrix:with-account (luna.test:*normal-user*)
      (let ((private-room (cl-matrix:room-create))
            (lunas-before-token (cl-matrix:with-account (luna.test:*luna-user*) (cl-matrix:now-token)))
            (normal-before-token (cl-matrix:now-token)))
        (cl-matrix:user-invite (cl-matrix:username luna.test:*luna-user*) private-room)
        (let ((listener (cl-matrix:with-account (luna.test:*luna-user*) (luna:start-listening :sync-rate 2 :sync-token lunas-before-token))))
          (push listener luna.test:*listeners*)
          (true (luna.test:wait-until
                 private-room
                 (lambda (e)
                   (and (string= (cl-matrix:username luna.test:*luna-user*) (jsown:val e "sender"))
                        (string= "m.room.member" (jsown:val e "type"))
                        (string= "join" (jsown:filter e "content" "membership"))))
                 :sync-token normal-before-token)))))))
