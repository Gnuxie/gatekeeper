(asdf:defsystem "gatekeeper.test"
  :version "0.0"
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :licence "NON-VIOLENT PUBLIC LICENSE v1+"
  :homepage "https://gnuxie.gitlab.io/gatekeeper/"
  :depends-on ("gatekeeper" "parachute" "hunchentoot" "luna.test")
  :serial t
  :components ((:file "package")
               (:file "webserver")
               (:file "test"))
  :description "homeserver invitation bot")
