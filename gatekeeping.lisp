#| This file is part of gatekeeper
   Copyright (C) 2019 Gnuxie <Gnuxie@protonmail.com>|#

(in-package #:gatekeeper)

(defvar *homeserver-whitelist* (list "breadpirates.chat"))
(defvar *target-endpoint* "http://loacalhost/request_token")

(define-condition gatekeeper-error (luna.framework:luna-error) ())

(defun localpart (user-id)
  (elt (nth-value 1 (cl-ppcre:scan-to-strings "@(.*)?:.*" user-id)) 0))

(defun post-invite (sender amount room-id)
  (let* ((m.room.member (cl-matrix:room-state room-id "m.room.member" sender))
         (avatar_url (and (jsown:keyp m.room.member "avatar_url") (jsown:val m.room.member "avatar_url")))
         (displayname (and (jsown:keyp m.room.member "displayname") (jsown:val m.room.member "displayname"))))

    (let* ((content
           `(:obj . 
             ,(remove-if #'null
                        `(("display_name" . ,displayname)
                          ("avatar_url" . ,avatar_url)
                          ("count" . ,amount)
                          ("localpart" . ,(localpart sender))
                          ("room_id" . ,room-id))
                        :key #'cdr)))
           (response
            (jsown:parse (drakma:http-request *target-endpoint*
                                              :content (jsown:to-json content)
                                              :content-type "application/json"
                                              :method :post))))
      (if (jsown:val-safe response "error")
          (jsown:val-safe response "error")
          (jsown:val-safe response "url")))))

(define-command-parser invite (name rest room-id event)
  "[AMOUNT]
Creates a link to invite a user this homeserver, AMOUNT is the number of users this link will be valid for."
  (declare (ignore name))
  (let ((sender (jsown:val event "sender")))
    (unless (member (cl-matrix:get-hostname sender) *homeserver-whitelist* :test #'string=)
      (error 'gatekeeper-error :description (format nil "your homeserver ~a is not in the whitelist." (cl-matrix:get-hostname sender))))

    (cl-ppcre:register-groups-bind (amount) ("([0-9]*)" rest)
      (let ((amount (if (= 0 (length amount))
                        1
                        (parse-integer rest))))
        (unless (and (> amount 0) (< amount 6))
          (error 'gatekeeper-error :description "amount must be no more than ~a" 5 ))

        (post-invite sender amount room-id)))))

(define-reporter invite (room-id event-id result)
  (if (stringp result)
      (report-summary room-id result result event-id)
      (v:error :invite "problem sending invite to ~a in response to ~a got~%~a~%" room-id event-id result)))
