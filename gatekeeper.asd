(asdf:defsystem "gatekeeper"
  :version "0.0"
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :licence "NON-VIOLENT PUBLIC LICENSE v1+"
  :homepage "https://gnuxie.gitlab.io/gatekeeper/"
  :depends-on ("luna.framework")
  :serial t
  :components ((:file "package")
               (:file "gatekeeping")
               (:file "hooks"))
  :description "homeserver invitation bot")
