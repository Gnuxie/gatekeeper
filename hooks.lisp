#| This file is part of gatekeeper
   Copyright (C) 2019 Gnuxie <Gnuxie@protonmail.com>|#

(defpackage #:gatekeeper.hooks
  (:use #:cl #:method-hooks #:cl-matrix.base-events #:gatekeeper))

(in-package #:gatekeeper.hooks)

(defhook invite-event gatekeeper-invite-join-hook (account room-id room-data)
         (when (find-if (lambda (e)
                          (and (string= (jsown:val e "type") "m.room.member")
                               (string= (jsown:val e "state_key") (cl-matrix:username account))
                               (string= (jsown:filter e "content" "membership") "invite")
                               (find (cl-matrix:get-hostname (jsown:val e "sender")) *homeserver-whitelist*
                                     :test #'string=)))
                        (jsown:filter room-data "invite_state" "events"))
           (cl-matrix:room-join room-id)))
